#pragma once

#include <list>
#include "Block.h"
#include "Drawable.h"

class BlockDrawer : Drawable
{
    const std::list<Block>& blocks;

public:
    BlockDrawer(const std::list<Block>& blocks);
    void draw();
    void drawBlock(const Block& block);
};

