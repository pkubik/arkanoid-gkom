#include "Block.h"

unsigned Block::lastId = 0;

Block::Block(const double& x, const double& y, const double& w, const double& h) :
    rect(x, y, w, h),
    id(lastId)
{
    ++lastId;
}

Vector2D Block::bounceOff(const Ball& ball)
{
    const auto& circle = ball.getCircle();
    /* (X,Y) quarter of rectangle to be checked */
    const int X = circle.getCenter().x >= rect.getCenter().x ? 1 : -1;
    const int Y = circle.getCenter().y >= rect.getCenter().y ? 1 : -1;
    const Vector2D rectCorner(
        X >= 0 ? rect.getRightX() : rect.getLeftX(),
        Y >= 0 ? rect.getTopY() : rect.getBottomY());
    const Vector2D circleCorner(
        X <= 0 ? circle.getRightX() : circle.getLeftX(),
        Y <= 0 ? circle.getTopY() : circle.getBottomY());
    /* does circle intersect vertical or horizontal bounding line of the rectangle */
    int cX = 0, cY = 0;
    if ((circleCorner.x - rectCorner.x) * X <= 0)
        cX = 1;
    if ((circleCorner.y - rectCorner.y) * Y <= 0)
        cY = 1;

    auto outX = 0.0;
    auto outY = 0.0;

    if (cX && cY)
    {
        /* is circle center directly vertically or horizontally aligned with the rectangle */
        int rX = 0, rY = 0;
        const auto dX = circle.getCenter().x - rectCorner.x;
        const auto dY = circle.getCenter().y - rectCorner.y;
        if (dX * X <= 0)
            rX = 1;
        if (dY * Y <= 0)
            rY = 1;
        if (rX || rY)
        {
            outX = rY * Y;
            outY = rX * X;
        }
        else
        {
            outX = dX;
            outY = dY;
        }

        --hitPoints;
    }

    return Vector2D(outX, outY);
}
