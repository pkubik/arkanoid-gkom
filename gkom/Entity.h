#pragma once

#include "Vector2D.h"

class Entity
{
protected:
    Vector2D position;

public:
    Entity(const double& x, const double& y) : position(x, y) {}
    void setCenter(const double& x, const double& y) { position.x = x; position.y = y; }
    void setCenter(const Vector2D& position) { this->position = position; }
    void setCenterX(const double& x) { position.x = x; }
    void setCenterY(const double& y) { position.y = y; }
    const Vector2D& getCenter() const { return position; }
};