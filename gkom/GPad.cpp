#include <Windows.h>
#include <gl\GL.h>
#include "GLUT.H"
#include "GPad.h"

void GPad::draw()
{
    //PAD
    static const GLfloat padDiffuse[] = { 0.15, 0.15, 0.7, 1.0 };
    auto& rect = pad.getRect();

    glPushMatrix();
    
    glMaterialfv(GL_FRONT, GL_DIFFUSE, padDiffuse);
    glTranslatef(rect.getCenter().x, rect.getCenter().y, 0.05f);
    glScalef(rect.getWidth(), rect.getHeight(), 0.2f);
    glutSolidCube(1.0f);

    glPopMatrix();

    //ARM
    static const GLfloat armDiffuse[] = { 0.0, 0.01, 0.2, 1.0 };
    const GLfloat X = rect.getCenter().x;
    const GLfloat Y = rect.getBottomY();
    const GLfloat Z2 = -0.2f;
    const GLfloat W = 0.04;
    const GLfloat L = 0.4f;
    const GLfloat M = 0.35f * (1.0f - X) / 2.0f;
    const GLfloat H = sqrt(L*L - M*M);
    const GLfloat SIN = H / L;
    const GLfloat R = W / SIN / 2.0f;
    const GLfloat F = W * SIN;

    GLfloat vertices[] = {
        X - R, Y, 0.0f,
        X + R, Y, 0.0f,
        X + M + R, Y - H, Z2,
        X + M + R - F, Y - H - W, Z2,
        10.0f, Y - H - W, Z2,
        10.0f, Y - H, Z2
    };

    GLubyte indices[] = {
        0, 3, 2,
        2, 1, 0,
        3, 4, 2,
        2, 4, 5
    };

    glEnableClientState(GL_VERTEX_ARRAY);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, armDiffuse);
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_BYTE, indices);
    glDisableClientState(GL_VERTEX_ARRAY);
}