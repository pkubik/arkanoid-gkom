#pragma once

#include "Rect.h"
#include "Ball.h"

class Block
{
    static unsigned lastId;
    unsigned id;
    unsigned typeId = 0;
    unsigned hitPoints = 1;
    Rect rect;

public:
    Block(const double& x, const double& y, const double& w, const double& h);
    unsigned getId() const { return id; }
    unsigned getTypeId() const { return typeId; }
    void setTypeId(unsigned typeId) { this->typeId = typeId; }
    const Rect& getRect() const { return rect; }
    Vector2D bounceOff(const Ball& ball);
    bool isDestroyed() const { return hitPoints == 0; }
    void setHitPoints(unsigned hp) { hitPoints = hp; }
    unsigned getHitPoints() const { return hitPoints; }
};

