#include "Ball.h"

void Ball::step(const double& seconds)
{
    circle.setCenter(circle.getCenter() + seconds * velocity);
}

void Ball::redirect(const Vector2D& v)
{
    const auto l = velocity.length();
    velocity = v;
    velocity.normalize();
    velocity *= l;
}

void Ball::bounceOffSurface(const Vector2D& normal)
{
    if (normal.length() > 0)
    {
        invertVelocity();
        velocity.mirror(normal);
    }
}