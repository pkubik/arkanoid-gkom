#include "Game.h"
#include <iostream>

Game::Game() :
    pad(0.0, -1.05, 0.7, 0.02),
    board(0.0, 0.0, 2.0, 2.0),
    ball(0.0, -0.6, 0.03, 0.02, 1.6)
{
    init();
}

void Game::init()
{
    generateBlocks();
}

void Game::generateBlocks()
{
    const double BLOCK_WIDTH = 0.28;
    const double BLOCK_HEIGHT = 0.12;
    const double MARGIN = 0.01;
    blocks.clear();
    const double FIRST_X = BLOCK_WIDTH / 2 + MARGIN;
    const double FIRST_Y = 0.05;
    for (int i = 0; FIRST_X + i * (BLOCK_WIDTH + MARGIN) < 0.8; ++i)
    {
        for (int j = 0; j < 7; ++j)
        {
            blocks.emplace_back(Block(
                FIRST_X + i * (BLOCK_WIDTH + MARGIN),
                FIRST_Y + j * (BLOCK_HEIGHT + MARGIN), 
                BLOCK_WIDTH, BLOCK_HEIGHT));
            blocks.back().setTypeId(j);
            if (j == 6)
                blocks.back().setHitPoints(2);
            blocks.emplace_back(Block(
                -FIRST_X - i * (BLOCK_WIDTH + MARGIN),
                FIRST_Y + j * (BLOCK_HEIGHT + MARGIN),
                BLOCK_WIDTH, BLOCK_HEIGHT));
            blocks.back().setTypeId(j);
            if (j == 6)
                blocks.back().setHitPoints(2);
        }
    }
}

void Game::cheat(int code)
{
    switch (code)
    {
    case 'c': if (!blocks.empty()) blocks.pop_back(); break;
    }
}

void Game::step(long long deltaTime)
{
    static const unsigned F = 10;

    if (!pause)
    {
        double seconds = deltaTime / 1000000000.0;

        for (unsigned i = 0; i < F; ++i)
        {
            ball.step(seconds / F);

            auto& circle = ball.getCircle();

            if (circle.isCollidingHorizontalLine(board.getRect().getTopY()))
                ball.invertVelocityY();

            if (circle.isCollidingHorizontalLine(board.getRect().getBottomY()))
            {
                pad.bounceOff(ball);
            }

            if (circle.isCollidingHorizontalLine(board.getRect().getBottomY() - 0.3))
                state = LOST;

            const bool collideLeft = circle.isCollidingVerticalLine(board.getRect().getLeftX());
            const bool collideRight = circle.isCollidingVerticalLine(board.getRect().getRightX());
            if ((collideLeft || collideRight) && circle.getTopY() >= board.getRect().getBottomY())
                ball.invertVelocityX();

            auto normal = Vector2D(0.0, 0.0);
            auto blit = blocks.begin();
            while (blit != blocks.end())
            {
                normal.interpolateDirection(blit->bounceOff(ball));
                if (blit->isDestroyed())
                    blocks.erase(blit++);
                else
                    ++blit;
            }
            ball.bounceOffSurface(normal);
        }

        if (blocks.empty())
        {
            togglePause();
            state = WON;
        }
    }
}