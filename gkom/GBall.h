#pragma once

#include "Drawable.h"
#include "Ball.h"

class GBall : public Drawable
{
    const Ball& ball;

public:
    GBall(const Ball& ball) : ball(ball) {}
    virtual void draw();
};

