#include "GBoard.h"
#include "GLUT.H"

GBoard::GBoard(const Board& board) : board(board)
{
}

void GBoard::draw()
{
    static const GLfloat backgroundDiffuse[] = { 0.7, 0.8, 0.9, 0.8 };
    static const GLfloat borderDiffuse[] = { 0.2, 0.3, 0.8, 1.0 };

    auto& rect = board.getRect();

    glPushMatrix();

    glMaterialfv(GL_FRONT, GL_DIFFUSE, backgroundDiffuse);
    glTranslatef(0.0f, 0.0f, -0.1f);
    glScalef(rect.getWidth(), rect.getHeight(), 0.1f);
    glutSolidCube(1.0f);

    glPopMatrix();

    glPushMatrix();

    glMaterialfv(GL_FRONT, GL_DIFFUSE, borderDiffuse);
    glTranslatef(rect.getCenter().x - 0.025 - rect.getWidth() / 2, 0.0f, 0.0f);
    glScalef(0.05f, rect.getHeight(), 0.3f);
    glutSolidCube(1.0f);

    glPopMatrix();

    glPushMatrix();

    glMaterialfv(GL_FRONT, GL_DIFFUSE, borderDiffuse);
    glTranslatef(rect.getCenter().x + 0.025 + rect.getWidth() / 2, 0.0f, 0.0f);
    glScalef(0.05f, rect.getHeight(), 0.3f);
    glutSolidCube(1.0f);

    glPopMatrix();

    glPushMatrix();

    glMaterialfv(GL_FRONT, GL_DIFFUSE, borderDiffuse);
    glTranslatef(0.0f, rect.getCenter().y + 0.025 + rect.getHeight() / 2, 0.0f);
    glScalef(rect.getWidth() + 0.1, 0.05f, 0.3f);
    glutSolidCube(1.0f);

    glPopMatrix();
}
