#include <Windows.h>
#include <gl/GL.h>
#include "GBall.h"
#include "GLUT.H"

void GBall::draw()
{
    static const GLfloat diffuse[] = { 0.8, 0.9, 1.0, 1.0 };
    auto& circle = ball.getCircle();

    glPushMatrix();

    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
    glTranslatef(circle.getCenter().x, circle.getCenter().y, 0.0f);
    glutSolidSphere(circle.getRadius(), 32, 32);

    glPopMatrix();
}
