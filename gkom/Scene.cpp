#include <Windows.h>
#include <GL/gl.h>
#include "GLUT.h"
#include <iostream>
#include <vector>
#include "lodepng.h"
#include "Scene.h"

Scene::Scene(const Game& game) :
    game(game),
    gpad(game.getPad()),
    gboard(game.getBoard()),
    gball(game.getBall()),
    blockDrawer(game.getBlocks())
{
}

void Scene::init()
{
    const GLfloat mat_ambient[] = { 0.3, 0.3, 0.3, 0.3 };
    const GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    const GLfloat light_position[] = { -1.0, -1.6, 6.0, 1.0 };
    const GLfloat light_position2[] = { -1.0, -1.6, 1.0, 1.0 };
    const GLfloat light_color[] = { 0.8, 0.8, 0.8, 1.0 };
    const GLfloat lm_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
    const GLfloat lm_lv[] = { 0.8, 0.8, 0.8, 1.0 };

    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialf(GL_FRONT, GL_SHININESS, 30.0);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_color);
    glLightfv(GL_LIGHT2, GL_POSITION, light_position2);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_color);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_color);
    glLightfv(GL_LIGHT1, GL_POSITION, light_position);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lm_lv);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, lm_lv);
    glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, lm_lv);

    glShadeModel(GL_SMOOTH);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_NORMALIZE);

    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    std::vector<unsigned char> image;
    unsigned width, height;
    unsigned error = lodepng::decode(image, width, height, "bricks.png");
    if (error != 0)
    {
        std::cout << "error " << error << ": " << lodepng_error_text(error) << std::endl;
        return;
    }

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

static const GLfloat WHITE_COLOR[] = { 1.0, 1.0, 1.0, 1.0 };
static const GLfloat GREEN_COLOR[] = { 0.1, 0.9, 0.1, 1.0 };
static const GLfloat RED_COLOR[] = { 0.9, 0.1, 0.1, 1.0 };
static const GLfloat BLACK_COLOR[] = { 0.0, 0.0, 0.0, 1.0 };

void Scene::display()
{
    const auto& pad = game.getPad();
    const auto& ball = game.getBall();

    glPushMatrix();

    float degX = ball.getCircle().getCenter().x / 8.0f * 90.0f;
    float degY = -pad.getRect().getCenter().x / 8.0f * 90.0f;
    glScalef(1.5f, 1.5f, 1.5f);
    glRotatef(-10.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(degY, 0.0f, 1.0f, 0.0f);

    switch (game.getState())
    {
    case Game::State::PLAYING:
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, WHITE_COLOR); break;
    case Game::State::LOST:
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, RED_COLOR); break;
    case Game::State::WON:
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GREEN_COLOR); break;
    }
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texID);
    glBegin(GL_TRIANGLE_FAN);
    glTexCoord2d(0.0, 0.0); glVertex3f(-6.0, -6.0, -0.5);
    glTexCoord2d(3.0, 0); glVertex3f(6.0, -6.0, -0.5);
    glTexCoord2d(3.0, 3.0); glVertex3f(6.0, 6.0, -0.8);
    glTexCoord2d(0, 3.0); glVertex3f(-6.0, 6.0, -0.5);
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, BLACK_COLOR);

    gboard.draw();
    gpad.draw();
    blockDrawer.draw();
    gball.draw();

    glPopMatrix();
}
