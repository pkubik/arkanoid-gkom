#include <Windows.h>
#include <gl\GL.h>
#include "GLUT.H"
#include "BlockDrawer.h"

BlockDrawer::BlockDrawer(const std::list<Block>& blocks) : blocks(blocks)
{
}

void BlockDrawer::draw()
{
    for (const Block& block : blocks)
        drawBlock(block);
}

void BlockDrawer::drawBlock(const Block& block)
{
    const GLfloat diffuse[] = { 0.1 + 0.5 * (block.getHitPoints()-1), 0.1 + 0.1 * block.getTypeId(), 0.7, 0.6 };
    auto& rect = block.getRect();

    glPushMatrix();

    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
    glTranslatef(rect.getCenter().x, rect.getCenter().y, 0.05f);
    glScalef(rect.getWidth(), rect.getHeight(), 0.08f);
    glutSolidCube(1.0f);

    glPopMatrix();
}