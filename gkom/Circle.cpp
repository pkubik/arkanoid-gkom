#include "Circle.h"

bool Circle::isCollidingHorizontalLine(const double& y) const
{
    return position.y > y ? (position.y - radius - y) < 0 : (position.y + radius - y) > 0;
}

bool Circle::isCollidingVerticalLine(const double& x) const
{
    return position.x > x ? (position.x - radius - x) < 0 : (position.x + radius - x) > 0;
}