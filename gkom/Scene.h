#pragma once

#include "Game.h"
#include "GPad.h"
#include "GBoard.h"
#include "GBall.h"
#include "BlockDrawer.h"

class Scene
{
    const Game& game;
    GPad gpad;
    GBoard gboard;
    GBall gball;
    BlockDrawer blockDrawer;
    GLuint texID = 0;

public:
    Scene(const Game& game);
    void init();
    void display();
};

