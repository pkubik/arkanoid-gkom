/*
* Pawel Kubik
* ISI
* GKOM
*/

#include <Windows.h>
#include <GL/gl.h>
#include "GLUT.h"
#include <iostream>
#include <chrono>
#include "Game.h"
#include "Scene.h"

int wWidth = 800;
int wHeight = 800;

Game game;
Scene scene(game);

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    scene.display();
    glutSwapBuffers();
}

void gameLoop()
{
    game.step(17000000);
    glClear(GL_COLOR_BUFFER_BIT);
    display();
}

void reshape(GLsizei w, GLsizei h)
{
    wWidth = w;
    wHeight = h;
    if (h > 0 && w > 0) {
        glViewport(0, 0, w, h);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        if (w <= h) {
            glOrtho(-2.25, 2.25, -2.25*h / w, 2.25*h / w, -10.0, 10.0);
        }
        else {
            glOrtho(-2.25*w / h, 2.25*w / h, -2.25, 2.25, -10.0, 10.0);
        }
        glMatrixMode(GL_MODELVIEW);
    }
}

void mouseMotion(int x, int y)
{
    game.setPadX((double(x) - wWidth/2) / 240);
    //std::cout << game.getPad().getRect().getCenter().x << ", " << game.getPad().getRect().getCenter().y << std::endl;
    gameLoop();
}

void mouseClick(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        std::cout << "Mouse Click." << std::endl;
        if (game.getState() == Game::State::PLAYING)
            game.togglePause();
        else
            game = Game();
    }
}

void keyPress(unsigned char key, int x, int y)
{

    if (key == 27)
        exit(0);
    else if (key == ' ')
    {
        if (game.getState() == Game::State::PLAYING)
            game.togglePause();
        else
            game = Game();
    }
    else if (key == 'r')
        game = Game();
    else
        game.cheat(key);

}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);

    glutInitWindowPosition(0, 0);
    glutInitWindowSize(wWidth, wHeight);

    glutCreateWindow("GPOB: OpenGL");

    glutDisplayFunc(gameLoop);
    glutIdleFunc(gameLoop);
    glutReshapeFunc(reshape);
    glutPassiveMotionFunc(mouseMotion);
    glutKeyboardFunc(keyPress);
    glutMouseFunc(mouseClick);

    scene.init();
    glutMainLoop();

    return 0;
}
