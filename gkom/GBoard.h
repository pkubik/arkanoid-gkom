#pragma once

#include "Drawable.h"
#include "Board.h"

class GBoard : Drawable
{
    const Board& board;

public:
    GBoard(const Board& board);
    void draw();
};

