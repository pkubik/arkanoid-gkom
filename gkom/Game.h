#pragma once

#include <list>
#include "Pad.h"
#include "Board.h"
#include "Ball.h"
#include "Block.h"

class Game
{
public:
    enum State { LOST, PLAYING, WON };

private:
    Pad pad;
    Board board;
    Ball ball;
    std::list<Block> blocks;
    bool pause = true;
    State state = PLAYING;

    void init();
    void generateBlocks();
public:
    Game();
    const Pad& getPad() const { return pad; }
    const Board& getBoard() const { return board; }
    const Ball& getBall() const { return ball; }
    const std::list<Block>& getBlocks() const { return blocks; }

    void setPadX(const double& x) { pad.setX(x); }
    void step(long long deltaTime);
    void togglePause() { pause = !pause; }
    State getState() const { return state; }
    
    void cheat(int code);
};

