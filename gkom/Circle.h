#pragma once

#include "Entity.h"

class Circle : public Entity
{
    double radius;

public:
    Circle(const double& x, const double& y, const double& r) : Entity(x, y), radius(r) {}
    void setRadius(const double& r) { radius = r; }
    const double& getRadius() const { return radius; }
    double getLeftX() const { return position.x - radius / 2; }
    double getRightX() const { return position.x + radius / 2; }
    double getTopY() const { return position.y + radius / 2; }
    double getBottomY() const { return position.y - radius / 2; }
    bool isCollidingHorizontalLine(const double& y) const;
    bool isCollidingVerticalLine(const double& x) const;
};