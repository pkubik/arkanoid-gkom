#pragma once

#include "Rect.h"

class Board
{
    Rect rect;

public:
    Board(const double& x, const double& y, const double& width, const double& height) : rect(x, y, width, height) {}

    const Rect& getRect() const { return rect; }
};