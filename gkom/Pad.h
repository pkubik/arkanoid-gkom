#pragma once

#include "Vector2D.h"
#include "Rect.h"
#include "Ball.h"

class Pad
{
    Rect rect;

public:
    Pad(const double& x, const double& y, const double& width, const double& height) : rect(x, y, width, height) {}
    void setX(const double&x);
    const Rect& getRect() const { return rect; }
    void bounceOff(Ball& ball) const;
};