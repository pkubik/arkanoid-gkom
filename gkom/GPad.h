#pragma once

#include "Vector2D.h"
#include "Pad.h"
#include "Drawable.h"

class GPad : Drawable
{
    const Pad& pad;

public:
    GPad(const Pad& pad) : pad(pad) {}
    virtual void draw();
};