#pragma once

#include "Circle.h"

class Ball
{
    Circle circle;
    Vector2D velocity;

public:
    /* vX, vY: initial horizontal and vertical velocity */
    Ball(const double& x, const double& y, const double& radius, const double& vX, const double& vY) : circle(x, y, radius), velocity(vX, vY) {}
    const Circle& getCircle() const { return circle; }
    const Vector2D& getVelocity() const { return velocity; }
    void step(const double& seconds);
    void invertVelocity() { velocity *= -1; }
    void invertVelocityX() { velocity.x *= -1; }
    void invertVelocityY() { velocity.y *= -1; }
    void redirect(const Vector2D& v);
    void bounceOffSurface(const Vector2D& normal);
};