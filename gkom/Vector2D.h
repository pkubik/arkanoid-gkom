#pragma once

#include <cmath>

struct Vector2D
{
    double x = 0;
    double y = 0;
    
    Vector2D(double x, double y) : x(x), y(y) {}
    
    Vector2D& operator+=(const Vector2D& v)
    {
        x += v.x;
        y += v.y;
        return *this;
    }

    Vector2D& operator-=(const Vector2D& v)
    {
        x -= v.x;
        y -= v.y;
        return *this;
    }

    Vector2D& operator*=(const double& s)
    {
        x *= s;
        y *= s;
        return *this;
    }

    Vector2D& selfDot(const Vector2D& v)
    {
        x *= v.x;
        y *= v.y;
        return *this;
    }

    Vector2D& mirror(const Vector2D& v)
    {
        const double xp = (v.y * v.x * y + v.x * v.x * x) / (v.y * v.y + v.x * v.x);
        const double yp = (v.y * v.x * x + v.y * v.y * y) / (v.y * v.y + v.x * v.x);
        x = x + 2 * (xp - x);
        y = y + 2 * (yp - y);
        return *this;
    }

    Vector2D& interpolateDirection(const Vector2D& v)
    {
        const auto tl = length();
        const auto vl = v.length();
        if (tl == 0.0)
        {
            x = v.x;
            y = v.y;
        }
        else if (vl != 0.0)
        {
            x = (x * vl + v.x * tl) / (2 * vl * tl);
            y = (y * vl + v.y * tl) / (2 * vl * tl);
        }
        return *this;
    }

    double length() const
    {
        return sqrt(x*x + y*y);
    }

    Vector2D& normalize()
    {
        const auto l = length();
        x /= l;
        y /= l;
        return *this;
    }

    Vector2D dot(const Vector2D& v) const
    {
        return Vector2D(x * v.x, y * v.y);
    }
};

inline Vector2D operator+(const Vector2D& v1, const Vector2D& v2)
{
    return Vector2D(v1.x + v2.x, v1.y + v2.y);
}

inline Vector2D operator-(const Vector2D& v1, const Vector2D& v2)
{
    return Vector2D(v1.x - v2.x, v1.y - v2.y);
}

inline Vector2D operator*(const Vector2D& v, double s)
{
    return Vector2D(s * v.x, s * v.y);
}

inline Vector2D operator*(double s, const Vector2D& v)
{
    return Vector2D(s * v.x, s * v.y);
}