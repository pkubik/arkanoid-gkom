#include "Pad.h"
#include <cmath>

void Pad::setX(const double&x)
{
    if (x < -1.0)
        rect.setCenterX(-1.0);
    else if (x > 1.0)
        rect.setCenterX(1.0);
    else
        rect.setCenterX(x);
}

void Pad::bounceOff(Ball& ball) const
{
    const Circle& circle = ball.getCircle();
    if (rect.getLeftX() <= circle.getRightX() && rect.getRightX() >= circle.getLeftX())
    {
        ball.invertVelocity();
        const Vector2D& v = ball.getVelocity();
        double a = -(rect.getCenter().x - circle.getCenter().x) * 2/ rect.getWidth();
        ball.redirect(Vector2D(a, 1.0 - abs(a)));
    }
}