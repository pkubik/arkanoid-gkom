#pragma once

#include "Entity.h"

class Rect : public Entity
{
    Vector2D size;

public:
    Rect(const double& x, const double& y, const double& width, const double& height) : Entity(x, y), size(width, height) {}
    void setWidth(const double& w) { size.x = w; }
    void setHeight(const double& h) { size.y = h; }
    double getLeftX() const { return position.x - size.x / 2; }
    double getRightX() const { return position.x + size.x / 2; }
    double getTopY() const { return position.y + size.y / 2; }
    double getBottomY() const { return position.y - size.y / 2; }
    const double& getWidth() const { return size.x; }
    const double& getHeight() const { return size.y; }
};